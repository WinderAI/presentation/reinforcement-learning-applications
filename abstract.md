# Speaker Details

Name: Dr. Phil Winder

Position: CEO Winder Research

Website: [https://WinderResearch.com](https://WinderResearch.com)

Bio:

Dr. Phil Winder is a multidisciplinary software engineer and data scientist. As the CEO of Winder Research, a cloud-native data science consultancy, he helps startups and enterprises improve their data-based processes, platforms, and products. Phil specializes in implementing production-grade cloud-native machine learning and was an early champion of the MLOps movement. More recently, Phil has authored a book on Reinforcement Learning (RL) ([https://rl-book.com](https://rl-book.com)) which provides an in-depth introduction of industrial RL to engineers.

He has thrilled thousands of engineers with his data science training courses in public, private, and on the O’Reilly online learning platform. Phil’s courses focus on using data science in industry and cover a wide range of hot yet practical topics, from cleaning data to deep reinforcement learning. He is a regular speaker and is active in the data science community.

Phil holds a Ph.D. and M.Eng. in electronic engineering from the University of Hull and lives in Yorkshire, U.K., with his brewing equipment and family.

# Abstract

## Accompanying Image

![](robotic_arm.jpg)

## Industrial Applications of Reinforcement Learning

Reinforcement learning (RL), a sub-discipline of machine learning, has been gaining academic and media notoriety after hyped marketing "reveals" of agents playing various games. But these hide the fact that RL is immensely useful in may practical, industrial situations where hand-coding strategies or policies would be impractical or sub-optimal.

Following the theme of my new book ([https://rl-book.com](https://rl-book.com)), I present a rebuttal to the hyperbole by analysing five different industrial case studies from a variety of sectors. You will learn where RL can be applied, how to spot challenges that fit inside the RL paradigm, and what pitfalls to watch out for. You will learn that RL is more than an bot in a game; it is the next frontier in applied artificial intelligence.

I avoid using jargon to make this talk acceptable for a wider audience. I do expect that you have limited exposure to data science/machine learning in general.

